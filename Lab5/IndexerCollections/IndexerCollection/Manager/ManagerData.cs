﻿using IndexerCollection.Model;
using IndexerCollection.Repository;
using IndexerCollection.Repository.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerCollection.Manager
{
    class ManagerData
    {
        private int index;
        private Car _car;
        private CollectionCar _collectionCar;

        public ManagerData(CollectionCar collectionCar, Car car) {
            _collectionCar = collectionCar;
            _car = car;
            collectionCar.AddEvent += collectionCar.AddItem;
            collectionCar.SortEvent += collectionCar.SortEvent;
            collectionCar.RemoveEvent += collectionCar.RemoveItem;
        }

        
        public void Input() {
            try
            {
                Console.WriteLine("Enter brand");
                _car.Brand = Console.ReadLine();
                Console.WriteLine("Enter model");
                _car.Model = Console.ReadLine();
                Console.WriteLine("Enter price");
                _car.Price = int.Parse(Console.ReadLine());
            }
            catch (Exception ex) {
                Console.WriteLine($"{ex.Message}");
                Input();
            }
            
        }

        public void Manager() {
            while (true)
            {
                try
                {
                    _collectionCar.Deserializer();
                    Input();
                    _collectionCar.OnAddEvent(_car);
                    Console.WriteLine(_collectionCar);
                    Console.WriteLine("\nDelete index");
                    index = int.Parse(Console.ReadLine());
                    _collectionCar.OnRemoveEvent(index);
                    Console.WriteLine($"\nAfter collection {_collectionCar}");
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
