﻿using IndexerCollection.Manager;
using IndexerCollection.Model;
using IndexerCollection.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            new ManagerData(new CollectionCar(), new Car()).Manager();
            Console.ReadLine();
        }
    }
}
