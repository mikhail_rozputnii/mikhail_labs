﻿using IndexerCollection.Model;
using IndexerCollection.Repository.Event;
using IndexerCollection.Serializer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace IndexerCollection.Repository
{
    public class CollectionCar
    {
        public List<Car> car = new List<Car>();
        public Car this[int index]
        {
            get
            {
                if (index >= 0 && index < car.Count)
                {
                    return car[index];
                }
                else throw new Exception("Error!");
            }
            set
            {
                if (!(index >= 0 && index < car.Count))
                {
                    throw new Exception("Error!");
                }
                car[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return car.Count;
            }
        }

        public void AddItem(object sender, ItemEventArgs e)
        {
            car.Add(e.Car);
            Serializer();
        }

        public void RemoveItem(object sender, ItemEventArgs e)
        {
            car.RemoveAt(e.Index);
        }

        public void SortItem(object sender, ItemEventArgs e)
        {
            car.Sort();
        }



        #region OnSomeEvent
        public EventHandler<ItemEventArgs> AddEvent, SortEvent, RemoveEvent;

        public void OnAddEvent(Car e)
        {
            AddEvent?.Invoke(this, new ItemEventArgs(e));
        }

        public void OnSortEvent(ItemEventArgs e)
        {
            SortEvent?.Invoke(this, e);
        }

        public void OnRemoveEvent(int n)
        {
            SortEvent?.Invoke(this, new ItemEventArgs(n));
        }
        #endregion
        public override string ToString()
        {
            string cars = "";
            foreach (Car c in car)
            {
                cars += $"\n Brand:{c.Brand} \nModel:{c.Model} \nModel:{c.Price}\n";
            }
            return cars;
        }

        public void Serializer()
        {
            new XMLSerializer<List<Car>>().Seriaize(car);
        }

        public void Deserializer()
        {
            XMLSerializer<List<Car>> obj = new XMLSerializer<List<Car>>();
            car = obj.Deserialize();
        }
    }
}
