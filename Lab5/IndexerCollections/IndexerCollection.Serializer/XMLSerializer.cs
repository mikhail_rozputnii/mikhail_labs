﻿using IndexerCollection.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IndexerCollection.Serializer
{
    public class XMLSerializer<T> where T:class
    {
        public void Seriaize(List<Car> item)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<T>));
            using (FileStream fs = new FileStream("car.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, item);
            }
        }

        public List<T> Deserialize()
        {
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<T>));
                using (FileStream fs = new FileStream("car.xml", FileMode.OpenOrCreate))
                {

                    List<T> obj = (List<T>)formatter.Deserialize(fs);
                    return obj;
                }

            }
        }
    }
}
