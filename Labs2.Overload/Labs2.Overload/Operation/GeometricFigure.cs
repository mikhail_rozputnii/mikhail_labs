﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs2.Overload.Operation
{
    class GeometricFigure
    {
        public double Square(double sideA) {
            Console.WriteLine("квадрат");
            return sideA * sideA;
        }

        public double Square(double sideA, double sideB){
            Console.WriteLine("прямоугольник");
            return sideA * sideB;
        }

        public double Square(double sideA, double sideB, double sideC){
            Console.WriteLine("треугольник");
            double p;
            p = (sideA + sideB + sideC) / 2; 
            return Math.Sqrt(p*(p - sideA)*(p - sideB)*(p - sideC));
        }


    }
}
