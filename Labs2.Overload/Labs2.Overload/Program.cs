﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Labs2.Overload.Operation;

namespace Labs2.Overload
{
    class Program
    {
        static void Main(string[] args)
        {
            GeometricFigure gr = new GeometricFigure();
            Console.WriteLine(gr.Square(4));
            Console.WriteLine(gr.Square(4, 7));
            Console.WriteLine(gr.Square(4,7,10));
            Console.ReadKey();

        }
    }
}
