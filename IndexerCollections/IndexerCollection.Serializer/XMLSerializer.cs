﻿using IndexerCollection.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IndexerCollection.Serializer
{
    public class XMLSerializer
    {
        public void Seriaize(List<Car> item)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Car>));
            using (FileStream fs = new FileStream("car.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, item);
            }
        }

        public List<Car> Deserialize()
        {
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<Car>));
                using (FileStream fs = new FileStream("car.xml", FileMode.OpenOrCreate))
                {

                    List<Car> car = (List<Car>)formatter.Deserialize(fs);
                    return car;
                }

            }
        }
    }
}
