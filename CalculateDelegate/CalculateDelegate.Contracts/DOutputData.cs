﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate.Contracts
{
    public delegate void DOutputData(double valueOne, string operation, double valueTwo);
}
