﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate.Contracts
{
    public interface IArithmeticFunction
    {
        double Result { get;  set; }
        void CellOperation(double valueOne, string operation, double valueTwo);
        DOutputData Output { get; set; }
    }
}
