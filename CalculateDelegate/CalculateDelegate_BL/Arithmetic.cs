﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate_BL
{
    class Arithmetic
    {
        public double Divide(double numberOne, double numberTwo)
        {
            return numberOne / numberTwo;
        }

        public double Minus(double numberOne, double numberTwo)
        {
            return numberOne - numberTwo;
        }

        public double Multiply(double numberOne, double numberTwo)
        {
            return numberOne * numberTwo;
        }

        public double Plus(double numberOne, double numberTwo)
        {
            return numberOne + numberTwo;
        }
    }
}
