﻿using CalculateDelegate.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate_BL
{
    public class CallOperation : IArithmeticFunction
    {
        public DOutputData Output { get; set; }
        Arithmetic arithmetic;
        private double result;
        public double Result { get { return result; }  set { value = result; } }

        public void CellOperation(double valueOne, string operation, double valueTwo)
        {
            arithmetic = new Arithmetic();
            switch (operation) {
                case "+":
                    result=arithmetic.Plus(valueOne, valueTwo);
                    break;
                case "-":
                    result = arithmetic.Minus(valueOne, valueTwo);
                    break;
                case "*":
                    result = arithmetic.Multiply(valueOne, valueTwo);
                    break;
                case "/":
                    if (valueTwo != 0)
                    {
                        result = arithmetic.Divide(valueOne, valueTwo);
                    }break;
                default:
                    break;
            }
            Output(valueOne, operation, valueTwo);
        }
    }
}
