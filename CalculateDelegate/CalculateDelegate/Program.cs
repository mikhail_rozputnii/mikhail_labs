﻿using CalculateDelegate_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            var tool = new ManagerData(new CallOperation());
            while (true)
            {
                tool.InputData();
                tool.CellFunc();
            }
            //Console.ReadKey();
        }
    }
}
