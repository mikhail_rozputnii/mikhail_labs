﻿using CalculateDelegate.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDelegate
{
    class ManagerData
    {
        private double _valueOne;
        private string _symbols;
        private double _valueTwo;
        private readonly IArithmeticFunction _arithmetic;



        public ManagerData(IArithmeticFunction arithmetic)
        {
            _arithmetic = arithmetic;
            _arithmetic.Output += OutputData;
        }

        public void InputData()
        {
            try
            {
                Console.Write("Введите 1 число : ");
                _valueOne = double.Parse(Console.ReadLine());
                Console.Write("Введите символ операции \" +, -, *, / \": ");
                _symbols = Console.ReadLine();
                Console.Write("Введите 2 число ");
                _valueTwo = double.Parse(Console.ReadLine());
            }catch(Exception ex)
            {
                InputData();
            }
        }

        public void OutputData(double valueOne, string operation, double valueTwo)
        {
           double tempResult = _arithmetic.Result;
            Console.WriteLine($"{valueOne} {operation} {valueTwo} = {tempResult}"); 
        }

        public void CellFunc() {
            _arithmetic.CellOperation(_valueOne, _symbols, _valueTwo);
        }0
    }
}
