﻿using IndexerCollection.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerCollection.Repository.Event
{
    public class ItemEventArgs : EventArgs
    {
        public int Index { get; }
        public Car Car { get; }

        public ItemEventArgs(Car car, int index) {
           Car = car;
            Index = index;
        }
    }
}
