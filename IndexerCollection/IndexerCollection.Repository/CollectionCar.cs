﻿using IndexerCollection.Model;
using IndexerCollection.Repository.Event;
using System;
using System.Collections;
using System.Collections.Generic;

namespace IndexerCollection.Repository
{
    public class CollectionCar:IList<Car>
    {    
        public List<Car> car = new List<Car>();
        public Car this[int index] {
            get {
                if (index >= 0 && index < car.Count)
                {
                    return car[index];
                }
                else throw new Exception("Error!");
            }
            set {
                if (!(index >= 0 && index < car.Count)) {
                    throw new Exception("Error!");
                }  car[index] = value;
            }
        }

        public int Count {
            get {
                return car.Count;
            }
        }

        public bool IsReadOnly => throw new NotImplementedException();
        
        public void AddItem(object sender, ItemEventArgs e)
        {
            Add(e.Car);
        }

       public void RemoveItem(object sender, ItemEventArgs e)
        {
           RemoveAt(e.Index);
        }

        public void SortItem(object sender, ItemEventArgs e) {
            car.Sort();
        }


        public int IndexOf(Car item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, Car item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            car.RemoveAt(index);
        }

        public void Add(Car item)
        {
            car.Add(item);
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(Car item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Car[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Car item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Car> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }





        #region OnSomeEvent
        public EventHandler<ItemEventArgs> AddEvent, SortEvent, RemoveEvent;

        public void OnAddEvent(ItemEventArgs e)
        {
            AddEvent?.Invoke(this, e);
        }

        public void OnSortEvent(ItemEventArgs e)
        {
            SortEvent?.Invoke(this, e);
        }

        public void OnRemoveEvent(ItemEventArgs e)
        {
            SortEvent?.Invoke(this, e);
        }
        #endregion
           public override string ToString()
                {
                    foreach (Car c in car)
                    {
                        return $"\nBrand:{c.Brand} \nModel:{c.Model} \nModel:{c.Price}";
                    }
                    return $"Empty Collection";
                }
    }
}
