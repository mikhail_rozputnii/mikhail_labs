﻿using PopulationCountry.BL.EventList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PopulationCountry.BL.Collection
{
    public class PersonCollection<T> where T:struct
    {
        private T _name;
        public T Name { get { return _name; } set { _name = value; } }
        public List<T> pers = new List<T>();

        public event EventHandler<RemoveItemEventArgs> RemoveItemCollection;
        public event EventHandler<RemoveItemEventArgs> BeforeRemoveItemEvent;
        public event EventHandler<RemoveItemEventArgs> AfterRemoveItemEvent;

        public event EventHandler AddItemEvent;
        public event EventHandler BeforeAddItemEvent;
        public event EventHandler AfterAddItemEvent;

        public event EventHandler Sort;
        public event EventHandler OutputData;

        public virtual void OnAddItem()
        {
            if (BeforeAddItemEvent != null)
            {
                BeforeAddItemEvent(this, EventArgs.Empty);
                AddItemEvent?.Invoke(this, EventArgs.Empty);
                AfterAddItemEvent?.Invoke(this, EventArgs.Empty);
            }
        }

        public virtual void OnOutput()
        {
            OutputData?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnRemoveItems(RemoveItemEventArgs e)
        {
            if (BeforeRemoveItemEvent != null)
            {
                BeforeRemoveItemEvent(this, e);
                RemoveItemCollection?.Invoke(this, e);
                AfterRemoveItemEvent?.Invoke(this, e);
            }
        }

        public void BeforeRemoveItem(object sender, RemoveItemEventArgs e) {
            
        }

        public void AfterRemoveItem(object sender, RemoveItemEventArgs e)
        {

        }

        public void BeforeAddItem(object sender, EventArgs e)
        {

        }

        public void AfterAddItem(object sender, EventArgs e)
        {

        }


        public virtual void OnSortItems()
        {
            Sort?.Invoke(this, EventArgs.Empty);
        }

        public void AddItems(object sender, EventArgs e)
        {
            pers.Add(_name);
        }

        public void RemoveItems(object sender, RemoveItemEventArgs e)
        {
            pers.RemoveAt(e.Index);
        }

        public void SortItems(object sender, EventArgs e)
        {
            pers.Sort();
        }


        public void Output(object sender, EventArgs e)
        {
            foreach (T item in pers)
            {
                Console.Write($"{item}; ");
            }
        }
    }
}
