﻿using System;

namespace PopulationCountry.BL.EventList
{
    public class RemoveItemEventArgs : EventArgs
    {
        private  int _index;

        public RemoveItemEventArgs(int _index) {
            this._index = _index;
        }

        public  int Index { get { return _index; }}
    }
}
