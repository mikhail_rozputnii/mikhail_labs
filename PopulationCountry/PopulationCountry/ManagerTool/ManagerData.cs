﻿using PopulationCountry.BL.Collection;
using PopulationCountry.BL.EventList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PopulationCountry.ManagerTool
{
    public class ManagerData
    {
        PersonCollection<int> pers = new PersonCollection<int>();

        public void Register()
        {
            pers.BeforeAddItemEvent += pers.BeforeAddItem;
            pers.AddItemEvent += pers.AfterAddItem;
            pers.AfterAddItemEvent += pers.AddItems;
            pers.Sort += pers.SortItems;
            pers.BeforeRemoveItemEvent += pers.BeforeRemoveItem;
            pers.RemoveItemCollection += pers.RemoveItems;
            pers.AfterRemoveItemEvent += pers.RemoveItems;
            pers.OutputData += pers.Output;
        }



        public void InputData()
        {
            try
            {
                int size = 0;
                Console.Write("\nEnter size collection: ");
                size = int.Parse(Console.ReadLine());
                for (int i = 0; i < size; i++)
                {
                    Console.Write($"Enter item {i}:");
                    pers.Name = int.Parse(Console.ReadLine());
                    pers.OnAddItem();
                }
                pers.OnOutput();
            }
            catch (Exception ex) {
                Console.WriteLine($"Error! {ex.Message}");
                InputData();
            }
        }

        public void Sort() {
            pers.OnSortItems();
            Console.WriteLine("\nОтсортированый массив");
            pers.OnOutput();
            RemoveItem();
        }
        public void RemoveItem()
        {
            try
            {
                Console.Write("\nEnter item remove: ");
                int n = int.Parse(Console.ReadLine());
                pers.OnRemoveItems(new RemoveItemEventArgs(_index: n));
                Console.WriteLine($"You delete  item in index {n}");
                pers.OnOutput();
            }
            catch (Exception ex) {
                Console.WriteLine($"Error! {ex.Message}");
            }
        }
    }
}
