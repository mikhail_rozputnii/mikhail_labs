﻿using PopulationCountry.BL.Collection;
using PopulationCountry.BL.EventList;
using PopulationCountry.ManagerTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PopulationCountry
{
    class Program
    {
        static void Main(string[] args)
        {
                ManagerData mng = new ManagerData();
                mng.Register();
                while (true)
                {
                    mng.InputData();
                    mng.Sort();
                }
               
        }
    }
}
