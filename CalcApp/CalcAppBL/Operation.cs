﻿using System;

namespace CalcAppBL
{
    public class Operation
    {
        public int k;
        private double numberFirst;
        private double numberSecond;
        private double result;
        public double NumberFirst { get => numberFirst; set => numberFirst = value; }
        public double NumberSecond { get => numberSecond; set => numberSecond = value; }
        public double Result { get => result; set => result = value; }

        public double GetData(string ss) {
            return Double.Parse(ss);
        }



        public double OperationData() {
            switch (k) {
                case 0:
                    Result = NumberFirst + NumberSecond;
                    break;
                case 1:
                    Result = NumberFirst - NumberSecond;
                    break;
                case 2:
                    Result = NumberFirst * NumberSecond;
                    break;
                case 3:
                    Result = NumberFirst / NumberSecond;
                    break;
                default:
                    Result = 0;
                    break;
            }
            return Result;
        }

    }
}
