﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CalcAppBL;

namespace CalcApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Operation operation = new Operation();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            tb.Text = "";
            tbOp.Text = "";

        }

        private void res_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                operation.NumberSecond = operation.GetData(tb.Text);
                tb.Text = "";
                tbOp.Text = "";
                operation.OperationData();
                tb.Text = Convert.ToString(operation.Result);
            }
            catch (Exception) {
                MessageBox.Show("Enter number");
            }
        }

        private void bt_1_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_1.Content;
        }

        private void bt_2_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_2.Content;
        }

        private void bt_3_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_3.Content;
        }

        private void bt_4_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_4.Content;
        }

        private void bt_5_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_5.Content;
        }

        private void bt_6_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_6.Content;
        }

        private void bt_7_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_7.Content;
        }

        private void bt_8_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_8.Content;
        }

        private void bt_9_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_9.Content;
        }

        private void bt_0_Click(object sender, RoutedEventArgs e)
        {
            tb.Text += bt_0.Content;
        }

        private void bt_plus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                operation.k = 0;
                operation.NumberFirst = operation.GetData(tb.Text);
                tb.Text = "";
                tbOp.Text += bt_plus.Content;
            }
            catch (Exception)
            {
                MessageBox.Show("Enter number");
            }
        }

        private void bt_minus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                operation.k = 1;
                operation.NumberFirst = operation.GetData(tb.Text);
                tb.Text = "";
                tbOp.Text += bt_minus.Content;
            }
            catch (Exception)
            {
                tb.Text += bt_minus.Content;
            }
        }

        private void bt_multiply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                operation.k = 2;
                operation.NumberFirst = operation.GetData(tb.Text);
                tb.Text = "";
                tbOp.Text += bt_multiply.Content;
            }
            catch (Exception)
            {
                MessageBox.Show("Enter number");
            }
        }

        private void bt_divide_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                operation.k = 3;
                operation.NumberFirst = operation.GetData(tb.Text);
                tb.Text = "";
                tbOp.Text += bt_divide.Content;
            }
            catch (Exception)
            {
                MessageBox.Show("Enter number");
            }
        }
    }
}
