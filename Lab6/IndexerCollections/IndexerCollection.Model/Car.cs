﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerCollection.Model
{
    [Serializable]
    public struct Car:IComparable<Car>
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Price { get; set; }

        public int CompareTo(Car otherCar)
        {
            if (Price > otherCar.Price) {
                return 1;
            }
            if (Price < otherCar.Price)
            {
                return -1;
            }
            else
                return 0;
        }
    }
}
