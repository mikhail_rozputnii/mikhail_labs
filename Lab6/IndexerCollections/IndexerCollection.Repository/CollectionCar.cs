﻿using IndexerCollection.Model;
using IndexerCollection.Repository.Event;
using IndexerCollection.Serializer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IndexerCollection.Repository
{
    public class CollectionCar
    {
        public List<Car> cars = new List<Car>();
        public Car this[int index]
        {
            get
            {
                if (index >= 0 && index < cars.Count)
                {
                    return cars[index];
                }
                else throw new Exception("Error!");
            }
            set
            {
                if (!(index >= 0 && index < cars.Count))
                {
                    throw new Exception("Error!");
                }
                cars[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return cars.Count;
            }
        }

        public string LinqQuery() {
            var query = from c in cars
                        where c.Price > 15000
                        select c;
            string message="";
            foreach (Car car in query){
                message += $"\nBrand: {car.Brand}, Model: {car.Model} {car.Price}\n";
            }
            return message; 

        }
        public void AddItem(object sender, ItemEventArgs e)
        {
            cars.Add(e.Car);
            Serializer();
        }

        public void RemoveItem(object sender, ItemEventArgs e)
        {
            cars.RemoveAt(e.Index);
        }

        public void SortItem(object sender, ItemEventArgs e)
        {
            cars.Sort();
        }



        #region OnSomeEvent
        public EventHandler<ItemEventArgs> AddEvent, SortEvent, RemoveEvent;

        public void OnAddEvent(Car e)
        {
            AddEvent?.Invoke(this, new ItemEventArgs(e));
        }

        public void OnSortEvent(ItemEventArgs e)
        {
            SortEvent?.Invoke(this, e);
        }

        public void OnRemoveEvent(int n)
        {
            SortEvent?.Invoke(this, new ItemEventArgs(n));
        }
        #endregion
        public override string ToString()
        {
            string cars = "";
            foreach (Car c in this.cars)
            {
                cars += $"\n Brand:{c.Brand} \nModel:{c.Model} \nModel:{c.Price}\n";
            }
            return cars;
        }

        public void Serializer()
        {
            new XMLSerializer().Seriaize(cars);
        }

        public void Deserializer()
        {
            XMLSerializer obj = new XMLSerializer();
            cars = obj.Deserialize();
        }
    }
}
