﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using inheritanceGamePrimer.Heroes;

namespace inheritanceGamePrimer
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseHero lvOne = new BaseHero("KiberJohn", 20, 10,0);
            lvOne.Health();
            lvOne.Power();
            lvOne.PrintDataHero();

            LvlTwoHero lvTwo = new LvlTwoHero("RobotMik", 30,25,15);

            lvTwo.Health();
            lvTwo.Power();
            lvTwo.Damage();
            lvTwo.PrintDataHero();

            Console.ReadKey();
        }
    }
}
