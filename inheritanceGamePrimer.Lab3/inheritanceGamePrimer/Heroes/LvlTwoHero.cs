﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritanceGamePrimer.Heroes
{
    class LvlTwoHero:BaseHero
    {
        public LvlTwoHero(string nHero, double pHero, double hHero, int dHero)
            : base(nHero, pHero, hHero, dHero) {
            
        }

        public double Health() {
            return HealthHero -= 2;
        }

        public  double Power(){
            return PowerHero +=3;
        }
        public double Damage() {
            return PowerHero*DamageHero;
        }

    }
}
