﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritanceGamePrimer.Heroes
{
    class BaseHero
    {
        public string NameHero {get; set;}
        public double PowerHero { get; set; }
        public double HealthHero { get; set; }
        public double DamageHero { get; set; }

        public BaseHero(string nHero, double pHero, double hHero, int dHero)
        {
            NameHero = nHero;
            PowerHero = pHero;
            HealthHero = hHero;
            DamageHero = dHero;

        }

        public double Health() {
           return HealthHero = HealthHero - 3;
        }

        public double Power() {
            return PowerHero = HealthHero +1;
        }

        public void PrintDataHero() {
            Console.WriteLine("Name:{0}\n Health: {1} \n Power: {2} \n Damage {3}\n ",NameHero, HealthHero, PowerHero, DamageHero);
        }
    }
}
