﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Polyclinic.Model
{
    public class Doctor:INotifyPropertyChanged
        {
            private string _name;
            private string _surname;
            private string _age;
            private string _specialization;
            private int _experience;
            private string _station;

            public string Name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    OnPropertyChanged("");
                }
            }

            public string Surname
            {
                get { return _surname; }
                set
                {
                    _surname = value;
                    OnPropertyChanged("");
                }
            }

            public string Age
            {
                get { return _age; }
                set
                {
                    _age = value;
                    OnPropertyChanged("");
                }
            }

            public string Specialization
            {
                get { return _specialization; }
                set
                {
                    _specialization = value;
                    OnPropertyChanged("");
                }
            }

            public int Experience
            {
                get { return _experience; }
                set
                {
                    _experience = value;
                    OnPropertyChanged("");
                }
            }


        public string Station
        {
            get { return _station; }
            set
            {
                _station = value;
                OnPropertyChanged("");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged([CallerMemberName] string prop = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
