﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Polyclinic.Model
{
    public class Person:INotifyPropertyChanged
    {
        private string _name;
        private string _surname;
        private string _age;
        private string _address;
        private Doctor  _doctor;
        private int _phone;
        private string _passport;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("");
            }
        }

        public string Surname {
            get { return _surname; }
            set
            {
                _surname = value;
                OnPropertyChanged("");
            }
        }

        public string Age {
            get { return _age; }
            set
            {
                _age = value;
                OnPropertyChanged("");
            }
        }

        public string Address {
            get { return _address; }
            set {
                _address = value;
                OnPropertyChanged("");
            }
        }

        public Doctor Doctor{
            get { return _doctor; }
            set
            {
                _doctor = value;
                OnPropertyChanged("");
            }
        }

        public int Phone{
            get { return _phone; }
            set {
                _phone = value;
                OnPropertyChanged("");
            }
        }

        public string Passport
        {
            get { return _passport; }
            set
            {
                _passport = value;
                OnPropertyChanged("");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop ="") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}