﻿using Polyclinic.Model;
using Polyclinic.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Polyclinic.ViewModel
{
    public class DoctorViewModel : PersonsViewModel
    { 
        private Doctor _selectedDoctor;
        public Doctor SelectedDoctor
        {
            get { return _selectedDoctor; }
            set
            {
                _selectedDoctor = value;
                OnPropertyChanged("");
            }
        }

        private string _search;
        public string Search {
            get { return _search; }
            set
            {
                _search = value;
                OnPropertyChanged("");
            }
        }

        private Doctor _doctor;
        public Doctor Doctor
        {
            get { return _doctor; }
            set
            {
                _doctor = value;
                OnPropertyChanged("");
            }
        }


        public ObservableCollection<Doctor> Doctors { get; set; }
        private readonly IFileManager<ObservableCollection<Doctor>> _manager;

        public DoctorViewModel() {
            Doctor = new Doctor();
            _manager = new JsonManager<ObservableCollection<Doctor>> { FilePath = "personal.json" };
            Doctors = new ObservableCollection<Doctor>();
            Doctors = _manager.Open(new ObservableCollection<Doctor>());
            SaveCommandDoctor = new Command(Save);
            DeleteCommandDoctor = new Command(Delete);
            ChangeCommandDoctor = new Command(Change);
            ResetCommandDoctor = new Command(ResetDoctor);
            SearchCommandDoctor = new Command(SearchDoctor);
        }

        private void Delete(object o)
        {
            try
            {
                var currentPersonIndex = Persons.IndexOf(SelectedPerson);

                var nextDoctor = new Doctor();

                if (Doctors.Last() != SelectedDoctor)
                {
                    nextDoctor = Doctors.ElementAt(currentPersonIndex + 1);
                }
                else if (Doctors.First() == SelectedDoctor)
                {
                    Doctors.Add(nextDoctor);
                }
                else
                {
                    nextDoctor = Doctors.Last();
                }

                Doctors.Remove(SelectedDoctor);
                SelectedDoctor = Doctors.Last();
            }
            catch (Exception ex) { }
        }
        
        private void Change(object o) {
            Doctor = SelectedDoctor;
        }

        private void Save(object o)
        {
            _manager.Save(Doctors);
            SelectedDoctor = SelectedDoctor;
           
            if (SelectedDoctor != Doctor)
            {
                Doctors.Insert(0, Doctor);
                _manager.Save(Doctors);
                SelectedDoctor = Doctors.First();
                Doctor = new Doctor();
            }
            Doctor = new Doctor();
        }

        private void ResetDoctor(object o)
        {
            Doctor = new Doctor();
        }

        private void SearchDoctor(object o)
        {
            try { 
            Doctor = Doctors.First(obj => obj.Station == Search);
            }
            catch (Exception) {
                Search = $"Поиск завершился неудачно";
            }
        }

        public ICommand SaveCommandDoctor { get; set; }
        public ICommand ChangeCommandDoctor { get; set; }
        public ICommand DeleteCommandDoctor { get; set; }
        public ICommand SearchCommandDoctor { get; set; }
        public ICommand ResetCommandDoctor { get; set; }
    }
}
