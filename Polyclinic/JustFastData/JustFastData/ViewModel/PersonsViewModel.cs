﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Polyclinic.Model;
using Polyclinic.Repository;

namespace Polyclinic.ViewModel
{
    public class PersonsViewModel : BaseViewModel
    {
        private Person _selectedPerson;
        public Person SelectedPerson {
            get { return _selectedPerson; }
            set {
                _selectedPerson = value;
                OnPropertyChanged("");
            }
        }

        private Person _person;
        public Person Person
        {
            get { return _person; }
            set
            {
                _person = value;
                OnPropertyChanged("");
            }
        }

        private Person _personCheaked;
        public Person PersonCheaked
        {
            get { return _personCheaked; }
            set
            {
                _person = value;
                OnPropertyChanged("");
            }
        }

        private string _searchPerson;
        public string SearchPerson {
            get { return _searchPerson;  }
            set {
                _searchPerson = value;
                OnPropertyChanged("");
            }
        }

        public ObservableCollection<Person> Persons { get; set; }

        private readonly IFileManager<ObservableCollection<Person>> _manager;

        public PersonsViewModel()
        {
             _manager = new JsonManager<ObservableCollection<Person>> { FilePath = "persons.json" };
            Persons = new ObservableCollection<Person>();
            Persons = _manager.Open(new ObservableCollection<Person>());

            SaveCommand = new Command(Save);
            ChangeCommand = new Command(ChangePerson);
            DeleteCommand = new Command(Delete);
            SearchCommand = new Command(Search);
            ResetCommand = new Command(Reset);
        }

        private void Delete(object o)
        {
            var currentPersonIndex = Persons.IndexOf(SelectedPerson);

            var nextPerson = new Person();

            if (Persons.Last() != SelectedPerson) {
                nextPerson = Persons.ElementAt(currentPersonIndex + 1);
            }
            else if (Persons.First() == SelectedPerson) {
                Persons.Add(nextPerson);
            }
            else {
                nextPerson = Persons.Last();
            }

            Persons.Remove(SelectedPerson);
            SelectedPerson = Persons.First();
        }

        private void ChangePerson(object o)
        {
            Person = SelectedPerson;
        }

        private void Save(object o)
        {
            PersonCheaked = Persons.First(obj => obj.Passport == Person.Passport);
            if (PersonCheaked != null)
            {
                Person.Passport = "Error!";
            }
            else
            {
                _manager.Save(Persons);

                if (SelectedPerson != Person)
                {
                    Persons.Insert(0, Person);
                    _manager.Save(Persons);
                    Person = new Person();
                    SelectedPerson = Persons.First();
                }
                SelectedPerson = SelectedPerson;
                Person = new Person();
            }

        }

        private void Reset(object o)
        {
            Person = new Person();
        }

        private void Search(object o)
        {
            try
            {
                Person = Persons.First(obj => obj.Passport == SearchPerson);
            }
            catch (Exception)
            {
                SearchPerson = $"Поиск завершился неудачно";
            }
        }

        #region Command
        public ICommand ShutdownCommand { get; set; } = new Command(o => { Application.Current.Shutdown(); });
        public ICommand SaveCommand { get; set; }
        public ICommand ChangeCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ResetCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        #endregion
    }
}
