﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Polyclinic.Model;
using Polyclinic.Repository;

namespace Polyclinic.ViewModel
{
    public class PersonsViewModel : BaseViewModel
    {
        private Person _selectedPerson;
        public Person SelectedPerson {
            get { return _selectedPerson; }
            set {
                _selectedPerson = value;
                OnPropertyChanged("");
            }
        }

        public ObservableCollection<Person> Persons { get; set; }

        private readonly IFileManager<ObservableCollection<Person>> _manager;

        public PersonsViewModel()
        {
             _manager = new JsonManager<ObservableCollection<Person>> { FilePath = "persons.json" };
            Persons = new ObservableCollection<Person>();
            Persons = _manager.Open(new ObservableCollection<Person>());
            SaveCommand = new Command(Save);
            CreateCommand = new Command(Create);
            DeleteCommand = new Command(Delete);
        }

        private void Delete(object o)
        {
            var currentPersonIndex = Persons.IndexOf(SelectedPerson);

            var nextPerson = new Person();

            if (Persons.Last() != SelectedPerson) {
                nextPerson = Persons.ElementAt(currentPersonIndex + 1);
            }
            else if (Persons.First() == SelectedPerson) {
                Persons.Add(nextPerson);
            }
            else {
                nextPerson = Persons.Last();
            }

            Persons.Remove(SelectedPerson);
            SelectedPerson = Persons.Last();
        }

        private void Create(object o)
        {
            
            Person person = new Person();
            Persons.Insert(0, person);
            SelectedPerson = Persons.Last();
        }

        private void Save(object o)
        {
            _manager.Save(Persons);
        }

        #region Command
        public ICommand ShutdownCommand { get; set; } = new Command(o => { Application.Current.Shutdown(); });
        public ICommand SaveCommand { get; set; }
        public ICommand CreateCommand { get; set; } 
        public ICommand DeleteCommand { get; set; }
        #endregion
    }
}
