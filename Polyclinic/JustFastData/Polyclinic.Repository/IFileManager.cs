﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polyclinic.Repository
{
    public interface IFileManager<T> where T : class
    {
        void Save(T obj);
        T Open(T defValue);
    }
}
