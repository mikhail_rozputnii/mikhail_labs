﻿using System.IO;
using Newtonsoft.Json;

namespace Polyclinic.Repository
{
    public class JsonManager<T> : IFileManager<T> where T : class
    {
        public string FilePath { get; set; }

        public T Open(T defValue)
        {
            if (File.Exists(FilePath)) {
                var json = File.ReadAllText(FilePath);
                return JsonConvert.DeserializeObject<T>(json);
            }

            return defValue;
        }

        public void Save(T obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            File.WriteAllText(FilePath, json);
        }
    }
}