﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InputData;
using OperationBL.ArirhmeticBL;
using OperationBL.GeometricBL;




namespace myCalcAbstract
{
    class Program
    {
        static void Main(string[] args)
        {
            MainOperation();
            Console.ReadLine();
        }

        public static void MainOperation() {
            while (true)
            {
                Input.CheckOperation();
                switch (Input.CheckOp)
                {
                    case 1:
                        var _inpArithmetic = new Arithmetic();
                        _inpArithmetic.InputData();
                        var _arithmetic = new ArithmeticBL(_inpArithmetic.One, _inpArithmetic.Symbol, _inpArithmetic.Two);
                        _arithmetic.Calculate();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine(_arithmetic.OutData());
                        break;
                    case 2:
                        var _inpGeometric = new Geometric();
                        _inpGeometric.InputData();
                        var _geometric = new GeometricBL(_inpGeometric.One, _inpGeometric.GeometricOp);
                        _geometric.Calculate();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine(_geometric.OutData());
                        break;
                    default:
                        Console.WriteLine("Ошибка! \n");
                        break;
                }
            }
        }
    }
}
