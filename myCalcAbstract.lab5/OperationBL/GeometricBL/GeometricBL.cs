﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationBL.GeometricBL
{
    public class GeometricBL
    {
        private string _exception;
        public string Symbol { get ; set; }
        public double Number { get ; set ; }
        public double Result { get; set; }

        public GeometricBL(double _one, string _symbol){
            Number = _one;
            Symbol = _symbol;
            Calculate();
        }

        public double Cos(){
            return Math.Cos(Number);
        }

        public double Sin(){
            return Math.Sin(Number);
        }


        public double Tan(){
            return Math.Tan(Number);
        }

        public void Calculate(){
            switch (Symbol)
            {
                case"cos":
                    Result=Cos();
                    break;
                case "sin":
                    Result = Sin();
                    break;
                case "tan":
                    Result = Tan();
                    break;
                default:
                    _exception = "Введеный  тип операции не поддерживаеться! \n";
                    break;
            }
        }

        public string OutData()
        {
            if (_exception == null)
            {
                return string.Format(" \n \t\t {0}({1})={2} \n", Symbol, Number, Result);
            }
            else return _exception;
        }
    }
}
