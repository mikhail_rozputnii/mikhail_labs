﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationBL.ArirhmeticBL
{
    public class ArithmeticBL
    {
        private string _exception;
        public string Symbol { get; set; }
        public double NumberOne { get; set; }
        public double NumberTwo { get; set; }
        public double Result { get; set; }

        public ArithmeticBL(double _one, string _symbol, double _two) {
            NumberOne = _one;
            Symbol = _symbol;
            NumberTwo = _two;
            Calculate();
        }

        public double Plus() {
            return NumberOne + NumberTwo;
        }

        public double Minus() {
            return NumberOne - NumberTwo;
        }

        public double Multiply() {
            return NumberOne * NumberTwo;
        }

        public double Divide() {
            if (NumberTwo == 0) {
                _exception = "Деление на 0, невозможно!";
            }else return NumberOne / NumberTwo;
            return -1;
        }

        public void Calculate() {
            switch (Symbol) {
                case "+":
                    Result = Plus();
                    break;
                case "-":
                    Result = Minus();
                    break;
                case "*":
                    Result = Multiply();
                    break;
                case "/":
                    Result = Divide();
                    break;
                default:
                    _exception = "Введеный  тип операции не поддерживаеться! \n";
                    break;
            }

        }

        public string OutData() {
            if (_exception == null)
            {
                return string.Format("\n \t\t {0}{1}{2}={3} \n", NumberOne, Symbol, NumberTwo, Result);
            }
            else return _exception;
}
    }
}
