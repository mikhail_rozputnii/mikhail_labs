﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputData
{
   public  abstract class Input
    {
        public double One { get; set; }
        public string Symbol { get; set; }
        public double Two { get; set; }
        public static double CheckOp { get; set; }

        public static void CheckOperation() {
            try
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\t\t Выберите тип калькулятора! \n \t Арифметический - \"1\" Геометрический - \"2\": ");
                CheckOp = double.Parse(Console.ReadLine());
            }
            catch (Exception) {
            }
        }

        public abstract void InputData();
    }
}
