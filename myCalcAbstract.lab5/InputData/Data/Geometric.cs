﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputData
{
   public class Geometric : Input
    {
        private double _number;
        private string _gsymbol;

        public string GeometricOp { get=>_gsymbol; set=>_gsymbol=value; }
        public double Number{ get => _number; set => _number = value; }

        public override void InputData()
        {
            try{
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("\nВведите геометрическую операцию \" cos; sin; tan\": ");
                GeometricOp = Console.ReadLine();

                Console.Write("Введите число: ");
                Number= double.Parse(Console.ReadLine());
            }
            catch (FormatException) {
                Console.WriteLine("Данные введено неверно!");
            }
        }
    }
}
