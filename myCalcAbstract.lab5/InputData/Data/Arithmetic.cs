﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputData
{
   public class Arithmetic : Input
    {
        private double _one;
        private double _two;
        private string _symbol;

        public double One { get => _one; set => _one = value; }
        public string GeometricOp { get => _symbol; set => _symbol = value; }
        public double Two{ get => _two; set => _two = value; }


        public override void InputData(){
            try
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("\nВведите 1 число: ");
                One = double.Parse(Console.ReadLine());

                Console.Write("Введите символ операции \"+; -; *; / \": ");
                Symbol = Console.ReadLine();

                Console.Write("Введите 2 число: ");
                Two = double.Parse(Console.ReadLine());
            }
            catch (FormatException) {
                Console.WriteLine("Данные введено неверно!");
                InputData();
            }
        }
    }
}
