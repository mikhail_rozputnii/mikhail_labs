﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InOutData
{
   public interface IInOutData
    {
       
        double One { get; set; }
        double Two { get; set; }
        string Symbol { get; set; }
        string Aph { get; set; }
        void CheckOp();
        string OutData();
    }
}
