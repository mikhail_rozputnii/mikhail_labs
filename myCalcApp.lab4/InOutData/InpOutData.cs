﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InOutData
{
    public class InpOutData : IInOutData
    {
        private double _one;
        private double _two;
        private string _symbol;
        private string _aph;

        public double One { get => _one; set => _one = value; }
        public double Two { get => _two; set => _two = value; }
        public string Symbol { get => _symbol; set => _symbol = value; }
        public string Aph { get => _aph; set => _aph = value; }


        public void CheckOp() {
            string check;
            Console.Write("\n {0,60}", "a - Арифметический/ g - Геометрический: ");
            check = Console.ReadLine();
            if (check == "a")
            {
                Arithmetic();
            }
            else if (check == "g")
            {
                Geometric();
            }
            else
            {
                Console.WriteLine("Данные введены неверно!");
            }
        }

        public void Arithmetic() {
            try {
                Console.WriteLine("Введите 1 число");
                One = double.Parse(Console.ReadLine());
                Console.WriteLine("Введите арифметическую операцию  \"+\", \"-\",\"*\",\"/\": ");
                Symbol = Console.ReadLine();
                Console.WriteLine("Введите 2 число");
                Two = double.Parse(Console.ReadLine());
            } catch (FormatException)
            {
                Console.WriteLine("Данные введены неверно!");
                Arithmetic();
            }
        }

        public void Geometric() {
            try {
                Console.WriteLine("Введите геометрическую операцию \"cos; sin; tan\"");
                Aph = Console.ReadLine();

                Console.WriteLine("Введите число");
                One = double.Parse(Console.ReadLine());
            }
            catch (FormatException) {
                Console.WriteLine("Данные введены неверно!");
                Geometric();
            }
        }

        public string OutData() {
                return String.Format("{0,30}{1}{2}=", One, Symbol, Two);
        }
    }
}
