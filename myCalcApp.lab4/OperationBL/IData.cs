﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationBL
{
   public interface IData
    {
        double One { get; set; }
        double Two { get; set; }
        string Symbols { get; set; }
        double CalcOp();
    }
}
