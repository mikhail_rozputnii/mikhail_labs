﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationBL
{
    public class Operation : IData
    {
        public double One { get; set; }
        public double Two { get; set; }
        public string Symbols { get; set; }
      
        public Operation(double one, string symbols, double two) {
            One = one;
             Symbols = symbols;
            Two = two;
           
        }

        public double CalcOp() {
            switch (Symbols)
            {
                case "+":
                    return One+Two;
                case "-":
                    return One-Two;

                case "*":
                    return One*Two;
                case "/":
                    if (Two == 0)
                    {
                       Console.WriteLine("Деление на 0 невозможно!");
                        break;
                    }
                    return One/Two;
            }
            return 0;
        }
    }
}
