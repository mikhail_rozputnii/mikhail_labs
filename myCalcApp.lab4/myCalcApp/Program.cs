﻿using System;
using OperationBL;
using GeometricOperation;
using InOutData;
namespace myCalcApp
{
    class Program
    {
        static void Main(string[] args)
        {

            Program.check();
            Console.ReadKey();
        }

        private static void check()
        {
            
            while (true)
            {
                IInOutData id = new InpOutData();
                id.CheckOp();
                       
                if (id.Aph == null)
                {
                    IData aOp = new Operation(id.One, id.Symbol, id.Two); 
                    Console.WriteLine("{0}{1}",id.OutData(), aOp.CalcOp());
                }
                else
                {
                    IGData gOp = new GOperation(id.Aph, id.One);
                    if (id.Aph == "sin") { gOp.Check(eGOperation.sin); }
                    if (id.Aph == "cos") { gOp.Check(eGOperation.cos); }
                    if (id.Aph == "tan") { gOp.Check(eGOperation.tan); }
                    Console.WriteLine("{0,25}({1})={2}", id.Aph, id.One, gOp.Result);
                }
            }
        }
    }
}

