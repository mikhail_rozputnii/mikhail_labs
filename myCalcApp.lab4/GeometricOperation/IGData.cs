﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricOperation
{
   public interface IGData
    {
        string Aph { get; set; }
        double Numb { get; set; }
        double Result { get; set; }
        void Check(eGOperation op);
    }
}
