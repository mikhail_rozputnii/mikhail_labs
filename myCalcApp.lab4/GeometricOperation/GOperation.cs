﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricOperation
{
   public class GOperation : IGData
    {
        private double _resut;
        public string Aph { get; set; }
        public double Numb { get; set; }
        public double Result { get => _resut; set => _resut = value; }
        public GOperation(string _aph, double _numb){
            Aph = _aph;
            Numb = _numb;
        }

        public void Check(eGOperation op) {
            switch(op){
                case eGOperation.cos:
                    Result= Math.Cos(Numb);
                    break;
                case eGOperation.sin:
                    Result = Math.Sin(Numb);
                    break;
                case eGOperation.tan:
                    Result = Math.Tan(Numb);
                    break; 
            }
        }
    }
}
