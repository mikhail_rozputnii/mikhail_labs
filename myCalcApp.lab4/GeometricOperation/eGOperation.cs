﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricOperation
{
    public enum eGOperation
    {
        cos,
        sin,
        tan
    }
}
