﻿using System;
using RozputniiLabs.Students_1_.Models;

namespace RozputniiLabs.Students_1_
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            IDataProvider provider = new DataProvider();
            var student = provider.Input();
            provider.Output(student);
            Console.ReadKey();
        }
    }
}