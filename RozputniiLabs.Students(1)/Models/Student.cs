﻿namespace RozputniiLabs.Students_1_.Models
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string GroupName { get; set; }

        public override string ToString()
        {
            return $"Firs name: {FirstName}\nLast name: {LastName}\nAge: {Age}\nGroup name: {GroupName}";
        }
    }
}