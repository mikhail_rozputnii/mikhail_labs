﻿namespace RozputniiLabs.Students_1_.Models
{
    public interface IDataProvider
    {
        Student Input();
        void Output(Student student);
    }
}