﻿using System;

namespace RozputniiLabs.Students_1_.Models
{
    public class DataProvider : IDataProvider
    {
        public Student Input()
        {
            Console.WriteLine("Enter first name");
            var firstName = Console.ReadLine();
            Console.WriteLine("Enter last name");
            var lastName = Console.ReadLine();
            Console.WriteLine("Enter age");
            var age = int.Parse(Console.ReadLine() ?? "0");
            Console.WriteLine("Enter group name");
            var groupName = Console.ReadLine();
            return new Student
            {
                FirstName = firstName,
                LastName = lastName,
                Age = age,
                GroupName = groupName
            };
        }

        public void Output(Student student)
        {
            Console.WriteLine(student);
        }
    }
}