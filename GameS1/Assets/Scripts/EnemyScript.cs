﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public AudioClip hitSound;
    public Transform explosion;

    // Use this for initialization
    void Start () {
		
	}

    public int health = 1;
    void OnCollisionEnter2D(Collision2D theCollision)
    {
        if (theCollision.gameObject.name.Contains("laser"))
        {
            LaserScript laser = theCollision.gameObject.GetComponent("LaserScript") as
            LaserScript;
            health -= laser.damage;
            Destroy(theCollision.gameObject);
            GetComponent<AudioSource>().PlayOneShot(hitSound);
        }
        if (health <= 0)
        {
            if (explosion) ;
            {
                GameObject exploder = ((Transform)Instantiate(explosion,this.transform.position, this.transform.rotation)).gameObject;
                Destroy(exploder, 2.0f);
            }
            Destroy(this.gameObject);
        }

        GameController controller =GameObject.FindGameObjectWithTag("GameController").GetComponent("GameController") as GameController;
        controller.KilledEnemy();
        controller.IncreaseScore(10);
    }
    void Update () {
		
	}
}
