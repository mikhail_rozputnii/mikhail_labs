﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript: MonoBehaviour {

    public AudioClip shootSound;
    public float playerSpeed = 2.0f;
    private float currentSpeed = 0.0f;

    public Transform laser;
    public float laserDistance = 0.2f;
    public float timeBetweenFires = 0.3f;
    private float timeTilNextFire = 0.0f;
    public List<KeyCode> shootButton;

    public List<KeyCode> upButton;
    public List<KeyCode> downButton;
    public List<KeyCode> leftButton;
    public List<KeyCode> rightButton;

    private Vector3 lastMovement = new Vector3();
	
	void Update () {
        Rotation();
        Movement();
   
        foreach (KeyCode element in shootButton)
        {
            if (Input.GetKey(element) && timeTilNextFire < 0)
            {
                timeTilNextFire = timeBetweenFires;
                ShootLaser();
                break;
            }
        }
        timeTilNextFire -= Time.deltaTime;
    }


    void ShootLaser()
    {
        GetComponent<AudioSource>().PlayOneShot(shootSound);
        float posX = this.transform.position.x + (Mathf.Cos((transform.localEulerAngles.z -
        90) * Mathf.Deg2Rad) * -laserDistance);
        float posY = this.transform.position.y + (Mathf.Sin((transform.localEulerAngles.z -
        90) * Mathf.Deg2Rad) * -laserDistance);
        Instantiate(laser, new Vector3(posX, posY, 0), this.transform.rotation);
    }

    void Rotation() {

        Vector3 worldPos = Input.mousePosition;
        worldPos = Camera.main.ScreenToWorldPoint(worldPos);
        float dx = this.transform.position.x - worldPos.x;
        float dy = this.transform.position.y - worldPos.y;
        float angle = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.Euler(new Vector3(0, 0, angle + 90));
        this.transform.rotation = rot;
    }

    void Movement()
    {
        Vector3 movement = new Vector3();
        movement += MoveIfPressed(upButton, Vector3.up);
        movement += MoveIfPressed(downButton, Vector3.down);
        movement += MoveIfPressed(leftButton, Vector3.left);
        movement += MoveIfPressed(rightButton, Vector3.right);
        movement.Normalize();
        if (movement.magnitude > 0)
        {
            currentSpeed = playerSpeed;
            this.transform.Translate(movement * Time.deltaTime * playerSpeed,
            Space.World);
            lastMovement = movement;
        }
        else
        {
            this.transform.Translate(lastMovement * Time.deltaTime * currentSpeed,
            Space.World);
            currentSpeed *= 0.9f;
        }
    }

    Vector3 MoveIfPressed(List<KeyCode> keyList, Vector3 Movement)
    {
       foreach (KeyCode element in keyList)
        {
            if (Input.GetKey(element))
            {
                return Movement;
            }
        }
        return Vector3.zero;
    }

}
